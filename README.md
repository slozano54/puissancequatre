# Puissance Quatre

## Description
Propositions de jeux autour du Puissance Quatre. Une partie des propositions est issue d'un document Eduscol.

## Contributing
Les propositions de jeux sont les bienvenues.

## Authors and acknowledgment
Membres du groupe jeux IREM de Lorraine.

## License
CC BY-NC-SA 4.0 Copyright (c) [2022 - IREM DE LORRAINE - GROUPE JEUX](https://framagit.org/slozano54/puissancequatre/-/blob/master/LICENCE.md)

## Project status
Transfert sur LaForeEdu [https://forge.apps.education.fr/iremlorrainegroupejeux/puissancequatre](https://forge.apps.education.fr/iremlorrainegroupejeux/puissancequatre)
